import prefect
from prefect import task, Flow, Parameter
from prefect.executors.dask import DaskExecutor
from prefect.run_configs import KubernetesRun
from prefect.storage import GitLab
import time

@task
def say_hello(name):
    time.sleep(10)
    logger = prefect.context.get("logger")
    logger.info(f"Hello, {name}!")

with Flow("hello-flow") as flow:
    people = Parameter("people", default=["Arthur", "Ford", "Marvin"])
    say_hello.map(people)

flow.run_config = KubernetesRun(env={"EXTRA_PIP_PACKAGES": "scikit-learn matplotlib nltk scipy sklearn fuzzywuzzy"}, image="prefecthq/prefect:latest-python3.8")
flow.executor = DaskExecutor("tcp://pde-dask-scheduler:8786")
flow.storage = GitLab(
    repo="28642236",
    path="flows/log_test.py",
    ref="d0462e7f46b4aee3d2112a04abad44b98743f121"
)

flow.register(project_name="test")
